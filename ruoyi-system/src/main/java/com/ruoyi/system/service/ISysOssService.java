package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.SysOss;
import com.ruoyi.system.domain.SysOss;
import com.ruoyi.system.domain.bo.SysOssBo;
import com.ruoyi.system.domain.vo.SysOssVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * 文件上传 服务层
 *
 * @author Lion Li
 */
public interface ISysOssService {

    TableDataInfo<SysOssVo> queryPageList(SysOssBo sysOss, PageQuery pageQuery);

    List<SysOss> queryList(SysOssBo sysOss);

    List<SysOssVo> listByIds(Collection<Long> ossIds);

    SysOssVo getById(Long ossId);

    SysOss upload(MultipartFile file,String type,String parentId);

    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


    /**
     * 新增公告
     *
     * @param oss 公告信息
     * @return 结果
     */
    int insertOss(SysOss oss);

    /**
     * 修改公告
     *
     * @param oss 公告信息
     * @return 结果
     */
    int updateOss(SysOss oss);

}
